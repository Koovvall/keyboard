package main;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Koval on 2014-11-05.
 */
public class KeyMap {
    private HashMap<String,Key> map = new HashMap<String,Key>();
    private Connection connection = null;
    private Statement statement;


    public KeyMap(){
        init();
    }

    public String getKeyCode(String key){
        return map.get(key).getKey();
    }

    public Key getKey(String key){
        return map.get(key);
    }

    public void putKeyCode(String key, String value, boolean press, boolean release){
        map.put(key,new Key(key,value,press,release));
        saveToDatabase();//Nie chce mi sie sprawdzać czy istnieje juz w bazie, lepiej wszystko na nowo zapisać.
        saveToGrammar();

    }

    public void removeKeyCode(String key){
        map.remove(key);
        saveToDatabase();
        saveToGrammar();
    }

    private void init(){
        try{
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:our.db");
            statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS keyboard (key,value,press BOOL,release BOOL)");
            loadFromDatabase();
            saveToGrammar();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Metoda ładująca z bazy wartosci do hashmapy
     */
    private void loadFromDatabase(){
        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM keyboard");
            while(rs.next())
                map.put(rs.getString("key"),new Key(rs.getString("key"),rs.getString("value"),rs.getBoolean("press"),rs.getBoolean("release")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda zapisująca do bazy całą hashmape
     */
    private void saveToDatabase(){
        try {
            statement.executeUpdate("DELETE FROM keyboard");
            for(Map.Entry<String, Key> entry : map.entrySet()) {
                String key = entry.getKey();
                Key value = entry.getValue();

                int press = 1, release = 1;
                if(!value.getPress()) press = 0;
                if(!value.getRelease()) release = 0;

                statement.executeUpdate("INSERT INTO keyboard VALUES('"+key+"','"+value.getValue()+"'" +
                        ","+press+","+release+")");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda zapisująca do pliku grammar cała hashMape
     */
    private void saveToGrammar(){
        try {
            PrintWriter writer = new PrintWriter("our.gram");
            ArrayList<String> lines = getFileLines();
            for(String s : lines) writer.println(s);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metoda zwracająca całą zawartośc pliku .gram jako kolejne linie
     * @return
     */
    private ArrayList<String> getFileLines(){
        ArrayList<String> lines = new ArrayList<String>();
        lines.add("#JSGF V1.0;");
        lines.add("grammar hello;");
        lines.add("public <keys> = ");
        String lastLine = "(";

        for(Map.Entry<String, Key> entry : map.entrySet())
            lastLine += entry.getKey()+ " | ";

        int i = lastLine.lastIndexOf("|");
        if(i != -1)
            lastLine = lastLine.substring(0, i-1);

        lastLine += " );";

        lines.add(lastLine);

        return lines;
    }

    /**
     * Zakończenie połączenia z bazą
     */
    public void close(){
        try{
            if(connection != null)
                connection.close();
        }catch(SQLException e){
            System.err.println(e);
        }
    }

    /**
     * Pomocnicza metoda zwracająca listę (słowo do wypowiedzenia ==> kod przycisku klawiatury)
     * @return
     */
    public Vector<String> getList(){
        Vector<String> vector = new Vector<String>();
        for(Map.Entry<String, Key> entry : map.entrySet()) {
            String key = entry.getKey();
            Key value = entry.getValue();
            vector.add(key+" ==> "+value.getValue());
        }
        return vector;
    }
}
