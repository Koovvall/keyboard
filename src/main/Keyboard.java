package main;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by Koval on 2014-11-04.
 */
public class Keyboard {
    private Configuration configuration;
    private MyLiveSpeechRecognizer recognizer;
    private boolean recording = false;
    private KeyMap keyMap;

    public Keyboard(){
        init();
    }

    public void start(){
        if(isRecording()) return;

        Thread thread = new Thread(){
            @Override
            public void run() {
                super.run();
                try{
                    recognizer.startRecognition(true);
                }catch(Exception e){
                    //Jeżeli złapiemy tzn. ze nie wylaczyl się jeszcze ten wątek wcześniej. Ustawiamy by dalej był aktywny
                    setRecording(true);
                    return;
                }
                setRecording(true);
                SpeechResult result;

                while(recording){
                    result = recognizer.getResult();
                    /*for (WordResult r : result.getWords()) {
                        System.out.println(r.getWord().toString()+" "+r.getScore());
                        System.out.println("");
                    }*/
                    onRecognize(result.getHypothesis());
                }
                recognizer.stopRecognition();
            }
        };
        thread.start();
    }

    public void stop(){
        setRecording(false);
    }

    public void onRecognize(String result){
        if(isRecording()){
            try {
                System.out.println(result);
                Robot robot = new Robot();
                Key key = keyMap.getKey(result);
                if(key != null){//raczej nie może być nullem, bo to nasz gram ogranicza tą mape ;p
                    int keyNumber = Integer.parseInt(key.getValue());
                    if(key.getPress())
                        robot.keyPress(keyNumber);
                    if(key.getRelease())
                        robot.keyRelease(keyNumber);
                }
            } catch (AWTException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isRecording() {
        return recording;
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
    }

    public void setKeyMap(KeyMap keyMap){
        this.keyMap = keyMap;
    }

    public void init(){
        if(isRecording()) return;

        configuration = new Configuration();
        configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/acoustic/wsj");
        configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/acoustic/wsj/dict/cmudict.0.6d");

        String classpath = new File("").getAbsolutePath();
        configuration.setGrammarPath("file:/"+classpath);
        configuration.setGrammarName("our");
        configuration.setUseGrammar(true);
        try {
            recognizer = new MyLiveSpeechRecognizer(configuration);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateGrammar(){
        if(recognizer!=null) recognizer.updateGrammar();
    }
}
