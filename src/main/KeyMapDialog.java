package main;

import javax.swing.*;
import java.awt.event.*;

public class KeyMapDialog extends JDialog implements KeyListener, ActionListener{
    private JPanel contentPane;
    private JButton saveButton;
    private JTextField textKey;
    private JTextField textValue;
    private JList list;
    private JButton deleteButton;
    private JCheckBox checkBoxRelease;
    private JCheckBox checkBoxPress;
    private KeyMap keyMap;

    public KeyMapDialog(KeyMap keyMap) {
        setContentPane(contentPane);
        this.keyMap = keyMap;
        setModal(true);
        updateListData();

        textValue.addKeyListener(this);
        saveButton.addActionListener(this);
        deleteButton.addActionListener(this);

        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                onClose();
            }
        });
    }

    @Override
    public void keyTyped(KeyEvent e) {
        textValue.setText("");
    }

    @Override
    public void keyPressed(KeyEvent e) {
        textValue.setText("");
    }

    @Override
    public void keyReleased(KeyEvent e) {
        textValue.setText(e.getKeyCode()+"");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(saveButton)){
            String key = textKey.getText();
            String value = textValue.getText();
            boolean press = checkBoxPress.isSelected();
            boolean release = checkBoxRelease.isSelected();
            if(key.length() == 0 || value.length() == 0){
                JOptionPane.showMessageDialog(this, "Pola nie mogą być puste","Błąd",JOptionPane.ERROR_MESSAGE);
            }else{
                keyMap.putKeyCode(key,value,press,release);
                textKey.setText("");
                textValue.setText("");
                updateListData();
            }
        }else if(e.getSource().equals(deleteButton)){
            int index = list.getSelectedIndex();
            if(index != -1){
                String string = (String) list.getSelectedValuesList().get(0);
                int i = string.indexOf("==>");
                if(i != -1){
                    keyMap.removeKeyCode(string.substring(0,i-1));
                    updateListData();
                }

            }
        }
    }

    private void updateListData(){
        list.setListData(keyMap.getList());
    }

    public void onClose(){

    }


}
