package main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main extends JDialog implements ActionListener{
    private JPanel contentPane;
    private JButton exitButton;
    private JTextPane textPane1;
    private JButton startButton;
    private JButton settingsButton;
    private JLabel resultLabel;
    private Keyboard keyboard;
    private KeyMap keyMap = new KeyMap();

    public static void main(String[] args) {
        Main dialog = new Main();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void init(){
        setContentPane(contentPane);
        setSize(400, 400);
        setModal(true);
        getRootPane().setDefaultButton(exitButton);

        exitButton.addActionListener(this);
        startButton.addActionListener(this);
        settingsButton.addActionListener(this);
    }

    public Main() {
        this.init();
        keyboard = new Keyboard(){
            @Override
            public void onRecognize(String result) {
                super.onRecognize(result);
                resultLabel.setText(result);
            }
        };
        keyboard.setKeyMap(keyMap);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                stopRecording();
                keyMap.close();
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(exitButton)) {
            this.dispose();
        }else if(e.getSource().equals(startButton)){
            if(!keyboard.isRecording()){
                keyboard.start();
                startButton.setText("Stop");
            }else
                stopRecording();
        }else if(e.getSource().equals(settingsButton)){
            stopRecording();
            KeyMapDialog dialog = new KeyMapDialog(keyMap){
                @Override
                public void onClose() {
                    keyboard.updateGrammar();
                }
            };
            dialog.pack();
            dialog.setVisible(true);
        }
    }

    private void stopRecording(){
        keyboard.stop();
        startButton.setText("Start");
    }

}
