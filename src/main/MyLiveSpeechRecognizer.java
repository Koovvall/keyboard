package main;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;

import java.io.IOException;

/**
 * Created by Koval on 2014-11-16.
 */
public class MyLiveSpeechRecognizer extends LiveSpeechRecognizer {
    private Configuration configuration;
    /**
     * Constructs new live recognition object.
     *
     * @param configuration common configuration
     */
    public MyLiveSpeechRecognizer(Configuration configuration) throws IOException {
        super(configuration);
        this.configuration = configuration;
    }

    public void updateGrammar(){
        this.context.setGrammar(configuration.getGrammarPath(),configuration.getGrammarName());
    }
}
