package main;

/**
 * Created by Koval on 2014-11-16.
 */
public class Key {
    private String key, value;
    private boolean press = true, release = true;

    public Key(String key, String value, boolean press, boolean release){
        this.key = key;
        this.value = value;
        this.press = press;
        this.release = release;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean getPress() {
        return press;
    }

    public void setPress(boolean press) {
        this.press = press;
    }

    public boolean getRelease() {
        return release;
    }

    public void setRelease(boolean release) {
        this.release = release;
    }
}
